/*
> Formative Task 1
*/
drop database if exists formativedb;
create database formativedb;
use formativedb;

create table if not exists address (
	id int primary key auto_increment,
    street varchar(120) not null,
    city varchar(40) not null,
    province varchar(40) not null,
    country varchar(40) not null,
    zipCode varchar(10)
);

create table if not exists manufacturer (
	id int primary key auto_increment,
    `name` varchar(120) not null,
    addressId int not null,
    constraint FK_Address_Manufacturer foreign key (addressId) references address (id)
);

create table if not exists brand (
	id int primary key auto_increment,
    `name` varchar(120) not null
);

create table if not exists product (
	id int primary key auto_increment,
    artNumber varchar(60),
    `name` varchar(120) not null,
    `description` TEXT,
    manufactureId int not null,
    brandId int not null,
    stock int not null default 0,
    constraint FK_Manufacturer_Product foreign key (manufactureId) references manufacturer(id),
    constraint FK_Brand_Product foreign key (brandId) references brand(id)
);

create table if not exists valuta (
	id int primary key auto_increment,
    `code` varchar(3) not null,
    `name` varchar(40) not null
    
);

create table if not exists price (
	id int primary key auto_increment,
    productId int not null,
    valutaId int not null,
    amount decimal(10,2) not null default 0.00,
    constraint FK_Product_Price foreign key (productId) references product (id),
    constraint FK_Valuta_Price foreign key (valutaId) references valuta (id)
);