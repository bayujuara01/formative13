/*
> Formative Task 4
Product yang stocknya kosong, menampilkan data-data yang berkaitan
dengan (manufacturer, brand, address, price & valutanya.
*/

SELECT
	product.`name` as productName, 
    product.stock, 
	brand.`name` as brandName, 
    price.amount, 
    valuta.`code`,
    valuta.`name` as valuta,
    manufacturer.`name` as manufactureName, 
    CONCAT(address.street, ', ', address.city, ', ', address.province, ', ', address.country, ' ', address.zipCode) as fullAddress
	FROM product 
		JOIN brand ON product.brandId = brand.id
		JOIN manufacturer ON product.manufactureId = manufacturer.id
        JOIN address ON manufacturer.addressId = address.id
        JOIN price ON product.id = price.productId
        JOIN valuta ON price.valutaId = valuta.id
	WHERE product.stock <= 0;
		