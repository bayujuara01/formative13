/*
> Formative Task 5
Buat query yang dapat menampilkan semua data terkait (manufacturer, brand, address, price & valutanya)
dimana product di urut atas harga
*/

SELECT product.stock, 
	product.`name` as productName, 
	brand.`name` as brandName, 
    manufacturer.`name`, address.street, 
    address.city, 
    price.amount, 
    valuta.`code` #, brand.`name`, address.city
	FROM product 
		JOIN brand ON product.brandId = brand.id
		JOIN manufacturer ON product.manufactureId = manufacturer.id
        JOIN address ON manufacturer.addressId = address.id
        JOIN price ON product.id = price.productId
        JOIN valuta ON price.valutaId = valuta.id
	ORDER BY price.amount;