/*
> Formative Task 2
*/
insert into address(street, city, province, country, zipCode) values
('Jl. Raya Sawocangkring No. 02 Wonoayu', 'Sidoarjo', 'Jawa Timur', 'Indonesia', '61234'),
('Jl. Kapas Jaya No.25, Gading, Kec. Tambaksari', 'Surabaya', 'Jawa Timur', 'Indonesia', '60143');

insert into manufacturer(`name`, addressId) values
('PT Indo Lautan Makmur', 1),
('PT Mitra Nasional Kualitas', 2);

insert into brand(`name`) values 
('ILM'),
('ILM PREMIUM'),
('SAKURA'),
('MINAKU'),
('CEDEA');

insert into product (artNumber, `name`, `description`, manufactureId, brandId, stock) values 
('1234123400001', 'BINTANG ILM 500G', 'Olahan ikan bentuk bintang, berat 500 gram', 1, 1, 80),
('1234123400002', 'SUKOI ILM 500G', 'Olahan ikan bentuk lingkaran dengan titik bintang, berat 500 gram', 1, 1, 50),
('1234123400003', 'SCALLOP ILM 500G', 'Olahan ikan dengan sayur bentuk lingkaran, berat 500 gram', 1, 1, 30),
('1234123400004', 'TEMPURA ILM 500G', 'Olahan ikan bentuk tempura, berat 500 gram', 1, 1, 100),
('1234123400005', 'TAHU IKAN', 'Olahan ikan premium bentuk tahu, berat 450 gram', 1, 2, 0),
('1234123400006', 'NUGGET ICE CREAM', 'Nugget ikan bentuk es krim, isi 24', 1, 3, 0),
('1234123400007', 'BOLA IKAN', 'Olahan ikan premium, bentuk bakso, berat 500 gram', 2, 4, 10),
('1234123400008', 'BOLA UDANG', 'Olahan udang, bentuk bakso, berat 500 gram', 2, 4, 15),
('1234123400009', 'SCALLOP MINAKU', 'Olahan ikan premium dengan sayur, bentuk lingkaran, berat 500 gram', 2, 4, 20),
('1234123400010', 'CRAB STICK', 'Olahan daging kepiting, bentuk ,ie, isi 12', 2, 5, 10);

insert into valuta(`code`, `name`) values
('IDR', 'Rupiah'),
('USD', 'US Dollar');

insert into price(productId, valutaId, amount) values 
(1, 1, 11500.00),
(2, 1, 12000.00),
(3, 1, 11500.00),
(4, 1, 10500.00),
(5, 1, 25500.00),
(6, 1, 7500.00),
(7, 1, 22000.00),
(8, 1, 21000.00),
(9, 1, 23500.00),
(10, 1, 24000.00),
(1, 2, 1.15),
(2, 2, 1.20),
(3, 2, 1.15),
(4, 2, 1.05),
(5, 2, 2.55),
(6, 2, 0.75),
(7, 2, 2.20),
(8, 2, 2.10),
(9, 2, 2.35),
(10, 2, 2.40);


