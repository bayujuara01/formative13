/*
> Formative Task 3
Product yang stocknya kosong, menampilkan data-data yang berkaitan
dengan (manufacturer, brand, address, price & valutanya.
*/

SELECT product.stock, 
	product.`name` as productName, 
	brand.`name` as brandName, 
    manufacturer.`name`, address.street, 
    address.city, 
    price.amount, 
    valuta.`code` #, brand.`name`, address.city
	FROM product 
		JOIN brand ON product.brandId = brand.id
		JOIN manufacturer ON product.manufactureId = manufacturer.id
        JOIN address ON manufacturer.addressId = address.id
        JOIN price ON product.id = price.productId
        JOIN valuta ON price.valutaId = valuta.id
	WHERE product.stock <= 0;
		