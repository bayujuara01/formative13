/*
> Formative Task 6
Buat query yang dapat menampilkan semua data terkait (manufacturer, brand, address, price & valutanya) 
dimana data hasil di group atas valuta code & di urut dengan harga dari harga termahal => terendah
*/

SELECT
	product.`name` as productName,  
	product.stock, 
	brand.`name` as brandName, 
    price.amount, 
    valuta.`code`,
    manufacturer.`name`, address.street as address, 
    address.city,
    address.province
	FROM product 
		JOIN brand ON product.brandId = brand.id
		JOIN manufacturer ON product.manufactureId = manufacturer.id
        JOIN address ON manufacturer.addressId = address.id
        JOIN price ON product.id = price.productId
        JOIN valuta ON price.valutaId = valuta.id
	GROUP BY valuta.`code`
    ORDER BY price.amount DESC;